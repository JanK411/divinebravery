package de.jjt.divineBravery.gui;

import de.jjt.divineBravery.util.Shuffler;
import de.jjt.framework.util.ProxyConfig;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;

public class PreferencesScreen extends Application {

	private ProxyConfig proxyConfig = ProxyConfig.NO_PROXY;

	private ShuffleGodScreen shuffleGodScreen;

	private PreferencesScreenController controller;

	private Shuffler shuffler;

	@Override
	public void start(Stage primaryStage) {
		primaryStage.initStyle(StageStyle.UTILITY);
		GridPane page;
		try {
			URL resource = getClass().getResource("./PreferencesScreen.fxml");
			FXMLLoader loader = new FXMLLoader(resource);
			page = loader.load();
			this.controller = loader.getController();
			this.controller.setScreen(this);
			this.controller.initialize();
			this.controller.setShuffler(this.shuffler);
			this.controller.manageShufflerRelevantGuiAvailability();
			Scene scene = new Scene(page);
			primaryStage.setScene(scene);
			primaryStage.setTitle("Preferences");
			primaryStage.getIcons().add(new Image("file:icon.png"));
			primaryStage.setResizable(false);
			primaryStage.sizeToScene();
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setShuffleGodScreen(ShuffleGodScreen shuffleGodScreen) {
		this.shuffleGodScreen = shuffleGodScreen;
	}

	public ShuffleGodScreen getShuffleGodScreen() {
		return this.shuffleGodScreen;
	}

	public ProxyConfig getProxyConfig() {
		return this.proxyConfig;
	}

	public void setProxyConfig(ProxyConfig proxyConfig) {
		this.proxyConfig = proxyConfig;
	}

	public void setShufflerInController(Shuffler shuffler) {
		this.controller.setShuffler(shuffler);
	}

	public void setShuffler(Shuffler shuffler) {
		this.shuffler = shuffler;
	}

	public Shuffler getShuffler() {
		return this.shuffler;
	}
}
