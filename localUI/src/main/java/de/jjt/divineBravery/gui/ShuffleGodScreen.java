package de.jjt.divineBravery.gui;

import de.jjt.divineBravery.model.Ability;
import de.jjt.divineBravery.model.God;
import de.jjt.divineBravery.model.Item;
import de.jjt.divineBravery.model.concreteItems.ActiveItem;
import de.jjt.divineBravery.model.enums.EnumLane;
import de.jjt.divineBravery.util.Build;
import de.jjt.divineBravery.util.Shuffler;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

public class ShuffleGodScreen extends Application {

	private ShuffleGodScreenController controller;

	private Shuffler shuffler;

	@Override
	public void start(Stage primaryStage) {
		VBox page;
		try {
			URL resource = getClass().getResource("ShuffleGodScreen.fxml");
			FXMLLoader loader = new FXMLLoader(resource);
			page = loader.load();
			Scene scene = new Scene(page);
			primaryStage.setScene(scene);
			this.controller = loader.getController();
			this.controller.setScreen(this);
			primaryStage.setTitle("Divine Bravery");
			primaryStage.getIcons().add(new Image("icon.png"));
			primaryStage.show();
			primaryStage.setResizable(false);
			primaryStage.sizeToScene();
			initializeShuffler();
		} catch (IOException e) {
			// böser catch block >:(
			e.printStackTrace();
		}
	}

	/**
	 * erstellt einen Shuffler
	 */
	private void initializeShuffler() {
		this.setShuffler(Shuffler.create());
		shuffle();
	}

	/**
	 * setzt die vom Shuffler zufällig ausgewählten Inhalte in die GUI.
	 */
	public void shuffle() {
		final Build build = this.getShuffler().getCompleteBuild();
		final God god = build.getGod();
		final Ability ability = build.getAbility();
		final ActiveItem relic1 = build.getRelic1();
		final ActiveItem relic2 = build.getRelic2();
		final Item boot = build.getBoot();
		final Item item1 = build.getItem1();
		final Item item2 = build.getItem2();
		final Item item3 = build.getItem3();
		final Item item4 = build.getItem4();
		final Item item5 = build.getItem5();
		final EnumLane lane = build.getLane();

		this.controller.getGodImage().setImage(FxHelper.zaubereFxImageZuSmiteElement(god));
		this.controller.getGodLabel().setText(god.getName());
		this.controller.getAbilityImage().setImage(FxHelper.zaubereFxImageZuSmiteElement(ability));
		this.controller.getAbilityLabel().setText(ability.getName());
		this.controller.getRelic1Image().setImage(FxHelper.zaubereFxImageZuSmiteElement(relic1));
		this.controller.getRelic1Label().setText(relic1.getName());
		this.controller.getRelic2Image().setImage(FxHelper.zaubereFxImageZuSmiteElement(relic2));
		this.controller.getRelic2Label().setText(relic2.getName());
		this.controller.getBootElementImage().setImage(FxHelper.zaubereFxImageZuSmiteElement(boot));
		this.controller.getBootElementLabel().setText(boot.getName());
		this.controller.getItem1Image().setImage(FxHelper.zaubereFxImageZuSmiteElement(item1));
		this.controller.getItem1Label().setText(item1.getName());
		this.controller.getItem2Image().setImage(FxHelper.zaubereFxImageZuSmiteElement(item2));
		this.controller.getItem2Label().setText(item2.getName());
		this.controller.getItem3Image().setImage(FxHelper.zaubereFxImageZuSmiteElement(item3));
		this.controller.getItem3Label().setText(item3.getName());
		this.controller.getItem4Image().setImage(FxHelper.zaubereFxImageZuSmiteElement(item4));
		this.controller.getItem4Label().setText(item4.getName());
		this.controller.getItem5Image().setImage(FxHelper.zaubereFxImageZuSmiteElement(item5));
		this.controller.getItem5Label().setText(item5.getName());
		this.controller.getLaneLabel().setText(lane.toString());

	}

	public Shuffler getShuffler() {
		return this.shuffler;
	}

	public void setShuffler(Shuffler shuffler) {
		this.shuffler = shuffler;
	}

}
