package de.jjt.divineBravery.gui;

import de.jjt.divineBravery.model.SmiteElement;
import javafx.scene.image.Image;

import java.io.File;

public class FxHelper {

	/**
	 * Liefert das korrekte Bild zu gegebenem SmiteElement. Wenn dieses nicht
	 * existiert wird ein Default-Image geliefert.
	 *
	 * @param element
	 * 		SmiteElement, zu welchem das fx-Image erzeugt werden soll
	 *
	 * @return das anzuzeigende Bild zu gegebenem SmiteElement.
	 */
	public static Image zaubereFxImageZuSmiteElement(SmiteElement element) {
		final File imgFile = element.getImageFile();
		if (imgFile.exists()) {
			return new Image("file:///" + imgFile.getAbsolutePath());
		}
		return new Image(element.zaubereDefaultImageFileUrl());
	}
}
