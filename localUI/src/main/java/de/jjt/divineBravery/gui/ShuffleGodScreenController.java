package de.jjt.divineBravery.gui;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

public class ShuffleGodScreenController {

	private ShuffleGodScreen screen;

	@FXML
	void beendenAction() {
		this.getStage().close();
	}

	private Stage getStage() {
		return (Stage) this.beendenButton.getScene().getWindow();
	}

	/**
	 * Kopiert den aktuellen itembuild in die Zwischenablage.
	 */
	@FXML
	void copyToClipboardAction() {
		String toCopy = "";
		toCopy += "|-------------------------- Divine Bravery © JanK411 & Uschi003 --------------------------|\n";
		toCopy += "| God: " + this.godLabel.getText() + " w/ #" + this.abilityLabel.getText() + " @ "
				+ this.laneLabel.getText() + "\n";
		toCopy += "| Active 1: " + this.relic1Label.getText() + "\n";
		toCopy += "| Active 2: " + this.relic2Label.getText() + "\n";
		toCopy += "| Item 1: " + this.bootElementLabel.getText() + "\n";
		toCopy += "| Item 2: " + this.item1Label.getText() + "\n";
		toCopy += "| Item 3: " + this.item2Label.getText() + "\n";
		toCopy += "| Item 4: " + this.item3Label.getText() + "\n";
		toCopy += "| Item 5: " + this.item4Label.getText() + "\n";
		toCopy += "| Item 6: " + this.item5Label.getText() + "\n";

		final Toolkit toolkit = Toolkit.getDefaultToolkit();
		final Clipboard clipboard = toolkit.getSystemClipboard();
		final StringSelection strSel = new StringSelection(toCopy);
		clipboard.setContents(strSel, null);
	}

	/**
	 * Erstellt einen neuen PreferencesScreen.
	 */
	@FXML
	void openPreferencesScreenAction() {
		PreferencesScreen preferencesScreen = new PreferencesScreen();
		preferencesScreen.setShuffleGodScreen(this.screen);
		preferencesScreen.setShuffler(this.screen.getShuffler());
		Stage primaryStage = new Stage();
		primaryStage.initModality(Modality.APPLICATION_MODAL);
		preferencesScreen.start(primaryStage);
	}

	/**
	 * Tut tolle Dinge wenn bestimmte Tasten gedrückt werden.
	 *
	 * @param event
	 */
	@FXML
	public void keyPressedAction(KeyEvent event) {
		if (event.getCode().equals(KeyCode.P)) {
			openPreferencesScreenAction();
		} else if (event.getCode().equals(KeyCode.C) && event.isControlDown()) {
			copyToClipboardAction();
		} else if (event.getCode().equals(KeyCode.S)) {
			shuffleAction();
		}
	}

	/**
	 * Shufflet einen neuen Itembuild
	 */
	@FXML
	void shuffleAction() {
		this.screen.shuffle();
	}

	@FXML
	private ImageView item3Image;

	@FXML
	private Button preferencesButton;

	@FXML
	private ImageView item5Image;

	@FXML
	private ImageView item2Image;

	/**
	 * @return the item3Image
	 */
	public ImageView getItem3Image() {
		return this.item3Image;
	}

	/**
	 * @return the item5Image
	 */
	public ImageView getItem5Image() {
		return this.item5Image;
	}

	/**
	 * @return the item2Image
	 */
	public ImageView getItem2Image() {
		return this.item2Image;
	}

	/**
	 * @return the relic2Label
	 */
	public Label getRelic2Label() {
		return this.relic2Label;
	}

	/**
	 * @return the item2Label
	 */
	public Label getItem2Label() {
		return this.item2Label;
	}

	/**
	 * @return the item5Label
	 */
	public Label getItem5Label() {
		return this.item5Label;
	}

	/**
	 * @return the relic2Image
	 */
	public ImageView getRelic2Image() {
		return this.relic2Image;
	}

	/**
	 * @return the item1Image
	 */
	public ImageView getItem1Image() {
		return this.item1Image;
	}

	/**
	 * @return the item4Image
	 */
	public ImageView getItem4Image() {
		return this.item4Image;
	}

	/**
	 * @return the abilityImage
	 */
	public ImageView getAbilityImage() {
		return this.abilityImage;
	}

	/**
	 * @return the item4Label
	 */
	public Label getItem4Label() {
		return this.item4Label;
	}

	/**
	 * @return the item1Label
	 */
	public Label getItem1Label() {
		return this.item1Label;
	}

	/**
	 * @return the relic1Image
	 */
	public ImageView getRelic1Image() {
		return this.relic1Image;
	}

	/**
	 * @return the godLabel
	 */
	public Label getGodLabel() {
		return this.godLabel;
	}

	/**
	 * @return the abilityLabel
	 */
	public Label getAbilityLabel() {
		return this.abilityLabel;
	}

	/**
	 * @return the godImage
	 */
	public ImageView getGodImage() {
		return this.godImage;
	}

	/**
	 * @return the item3Label
	 */
	public Label getItem3Label() {
		return this.item3Label;
	}

	/**
	 * @return the relic1Label
	 */
	public Label getRelic1Label() {
		return this.relic1Label;
	}

	/**
	 * @return the bootElementImage
	 */
	public ImageView getBootElementImage() {
		return this.bootElementImage;
	}

	/**
	 * @return the bootElementLabel
	 */
	public Label getBootElementLabel() {
		return this.bootElementLabel;
	}

	/**
	 * @return the laneLabel
	 */
	public Label getLaneLabel() {
		return this.laneLabel;
	}

	@FXML
	private Label relic2Label;

	@FXML
	private Label item2Label;

	@FXML
	private Label item5Label;

	@FXML
	private ImageView relic2Image;

	@FXML
	private Button shuffleButton;

	@FXML
	private Button copyButton;

	@FXML
	private ImageView item1Image;

	@FXML
	private ImageView item4Image;

	@FXML
	private ImageView abilityImage;

	@FXML
	private Label item4Label;

	@FXML
	private Label item1Label;

	@FXML
	private ImageView relic1Image;

	@FXML
	private Label godLabel;

	@FXML
	private Label abilityLabel;

	@FXML
	private ImageView godImage;

	@FXML
	private Label item3Label;

	@FXML
	private Label relic1Label;

	@FXML
	private ImageView bootElementImage;

	@FXML
	private Label bootElementLabel;

	@FXML
	private Label laneLabel;

	@FXML
	private Button beendenButton;

	public void setScreen(ShuffleGodScreen screen) {
		this.screen = screen;
	}
}
