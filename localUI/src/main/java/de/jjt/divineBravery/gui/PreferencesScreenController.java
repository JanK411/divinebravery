package de.jjt.divineBravery.gui;

import de.jjt.divineBravery.model.enums.EnumShuffleFilter;
import de.jjt.divineBravery.util.Shuffler;
import de.jjt.divineBravery.util.fileHelper.Downloader;
import de.jjt.divineBravery.util.observer.Observer;
import de.jjt.framework.gui.ProxySettingsScreen;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ProgressBar;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;

public class PreferencesScreenController implements Observer {

	/**
	 * Läd Informationen & Bilder von smite.guru herunter und generiert eine
	 * XML-Datei mit nötigen Informationen. Wenn Quickdownload toggled ist
	 * werden die nötigen dokumente von Dropbox.com geladen.
	 */
	@FXML
	void downloadAction() {
		disableAllGuiElements(true);
		Downloader downloader = Downloader.getInstance();
		new Thread(() -> {
			try {
				downloader.downloadAndGenerateXML(this.screen.getProxyConfig(), this.downloadImagesCheckBox.isSelected(), this);
				this.shuffler = Shuffler.create();
				this.shufflerChanged = true;
			} catch (Exception e) {
				// TODO böser catch block >:(
				e.printStackTrace();
			} finally {
				repairDisabledElements();
			}
		}).start();
	}

	/**
	 * Aktiviert (für den Download deaktivierte) GUI-Elemente auf dem
	 * JavaFX-Thread.
	 */
	private void repairDisabledElements() {
		Platform.runLater(() -> {
			disableAllGuiElements(false);
			manageShufflerRelevantGuiAvailability();
		});
	}

	/**
	 * Öffnet den ProxySettingsScreen und speichert die eingegebene ProxyConfig
	 * danach im PreferencesScreen.
	 *
	 * @throws IOException
	 */
	@FXML
	void openProxySettings() throws IOException {
		ProxySettingsScreen proxySettingsScreen = new ProxySettingsScreen();
		proxySettingsScreen.start(new Stage());
		this.screen.setProxyConfig(proxySettingsScreen.getProxyConfig());
	}

	/**
	 * Beendet die Stage und räumt auf.
	 *
	 * @throws IOException
	 */
	@FXML
	void abbrechenAction() throws IOException {
		this.getStage().close();
		this.deleteTempFolder();
	}

	/**
	 * Öffnet den ShuffleGodScreen (erneut) und setzt den zuvor eingestellten
	 * Filter.
	 *
	 * @throws IOException
	 */
	@FXML
	void okayAction() throws IOException {
		this.getStage().close();
		deleteTempFolder();
		switch (this.grobSelectionComboBox.getValue()) {
			case allGods:
				this.shuffler.resetGodsList();
				break;
			case god:
				this.shuffler.GodsListToGod(this.feinSelectionComboBox.getValue());
				break;
			case role:
				this.shuffler.GodsListToRole(this.feinSelectionComboBox.getValue());
				break;
			case type:
				this.shuffler.GodsListToType(this.feinSelectionComboBox.getValue());
				break;
		}
		if (!this.shufflerChanged) {
			this.screen.getShuffleGodScreen().shuffle();
		} else {
			this.goToNewShuffleGodScreen();
		}

	}

	/**
	 * Passt Elemente in der FeinFilterCombobox an.
	 */
	@FXML
	void grobSelectedAction() {
		if (this.shuffler != null) {
			String[] feinFilterContent = this.shuffler.getFeinFilterContent(this.getGrobSelectionComboBox().getValue());
			ObservableList<String> obsFeinFilterContent = FXCollections.observableArrayList(feinFilterContent);
			this.getFeinSelectionComboBox().setItems(obsFeinFilterContent);
			this.getFeinSelectionComboBox().setValue(feinFilterContent[0]);
		}
	}

	/**
	 * Tut tolle Dinge wenn bestimmte Buchstaben getippt werden.
	 *
	 * @param event
	 */
	@FXML
	public void keyPressedAction(KeyEvent event) throws IOException {
		if (event.getCode().equals(KeyCode.P)) {
			openProxySettings();
		} else if (event.getCode().equals(KeyCode.D)) {
			downloadAction();
		}
	}

	/**
	 * Springt zu einem Eintrag in der FeinSelectionCombox, dessen
	 * Anfangsbuchstabe übereinstimmt. Wenn bereits ein Element mit diesem
	 * Anfangsbuchstaben markiert ist wird das Folgeelement mit dem selben
	 * Anfangsbuchstaben markiert.
	 *
	 * @param event
	 */
	@FXML
	public void feinFilterAction(KeyEvent event) {
		String s = jumpTo(event.getText(), this.feinSelectionComboBox.getValue(),
				this.feinSelectionComboBox.getItems());
		if (s != null) {
			this.feinSelectionComboBox.setValue(s);
		}
	}

	/**
	 * initialisiert die GUI.
	 */
	public void initialize() {
		this.grobSelectionComboBox.setItems(FXCollections.observableArrayList(EnumShuffleFilter.values()));
		this.grobSelectionComboBox.setValue(EnumShuffleFilter.allGods);
		this.feinSelectionComboBox.setItems(FXCollections.observableArrayList("allGods"));
		this.feinSelectionComboBox.setValue("allGods");
	}

	/**
	 * Deaktiviert GUI-Elemente für welche die Informationen über alle Gods
	 * nötig sind.
	 */
	public void manageShufflerRelevantGuiAvailability() {
		// TODO schickere Idee bekommen, ob man vom ShuffleGodScreen kommt oder
		// keine XML hatte
		if (this.shuffler == null) {
			setDisableShufflerRelevantGuiElements(true);
		} else {
			setDisableShufflerRelevantGuiElements(false);
		}
	}

	/**
	 * Deaktiviert alle GUI-Elemente (außer den abbrechen-Button), wenn
	 * disabled.
	 *
	 * @param disabled
	 * 		boolean ob Elemente deaktiviert werden sollen
	 */
	private void disableAllGuiElements(boolean disabled) {
		setDisableShufflerRelevantGuiElements(disabled);
		this.downloadButton.setDisable(disabled);
		this.okayButton.setDisable(disabled);
		this.proxySettingsButton.setDisable(disabled);
		this.feinSelectionComboBox.setDisable(disabled);
		this.grobSelectionComboBox.setDisable(disabled);
	}

	/**
	 * Deaktiviert okay-Button und beide ComboBoxes, wenn disabled.
	 *
	 * @param disabled
	 * 		boolean ob Elemente deaktiviert werden sollen
	 */
	private void setDisableShufflerRelevantGuiElements(boolean disabled) {
		this.okayButton.setDisable(disabled);
		this.grobSelectionComboBox.setDisable(disabled);
		this.feinSelectionComboBox.setDisable(disabled);
	}

	/**
	 * Springt zu einem Eintrag in der FeinSelectionCombox, dessen
	 * Anfangsbuchstabe übereinstimmt. Wenn bereits ein Element mit diesem
	 * Anfangsbuchstaben markiert ist wird das Folgeelement mit dem selben
	 * Anfangsbuchstaben markiert.
	 *
	 * @param keyPressed
	 * @param currentlySelected
	 * @param items
	 *
	 * @return
	 */
	private static String jumpTo(String keyPressed, String currentlySelected, List<String> items) {
		String key = keyPressed.toUpperCase();
		if (key.matches("^[A-Z]$")) {
			// Only act on letters so that navigating with cursor keys does not
			// try to jump somewhere.
			boolean letterFound = false;
			boolean foundCurrent = currentlySelected == null;
			for (String s : items) {
				if (s.toUpperCase().startsWith(key)) {
					letterFound = true;
					if (foundCurrent) {
						return s;
					}
					foundCurrent = s.equals(currentlySelected);
				}
			}
			if (letterFound) {
				return jumpTo(keyPressed, null, items);
			}
		}
		return null;
	}

	/**
	 * erstellt einen neuen funktionalen ShuffleGodScreen.
	 */
	private void goToNewShuffleGodScreen() {
		ShuffleGodScreen shuffleGodScreen = new ShuffleGodScreen();
		shuffleGodScreen.start(new Stage());
		shuffleGodScreen.setShuffler(this.shuffler);
	}

	/**
	 * Löscht heruntergeladene Temporäre Dateien
	 *
	 * @throws IOException
	 */
	private void deleteTempFolder() throws IOException {
		File tempFolder = new File("Temp");
		if (tempFolder.exists()) {
			Files.walkFileTree(tempFolder.toPath(), new FileVisitor<Path>() {

				@Override
				public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {
					Files.delete(file);
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFileFailed(final Path file, final IOException e) {
					return handleException(e);
				}

				private FileVisitResult handleException(final IOException e) {
					e.printStackTrace();
					return FileVisitResult.TERMINATE;
				}

				@Override
				public FileVisitResult postVisitDirectory(final Path dir, final IOException e) throws IOException {
					if (e != null)
						return handleException(e);
					Files.delete(dir);
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
					return FileVisitResult.CONTINUE;
				}
			});
		}
	}

	/**
	 * Aktualisiert die Progressbar
	 *
	 * @param percentage
	 * 		setzt die Progressbar auf diesen Prozentwert. 1 == 100% #BWL
	 */
	@Override
	public void update(double percentage) {
		System.out.println(percentage);
		Platform.runLater(() -> this.progressBar.setProgress(percentage));
	}

	private Stage getStage() {
		return (Stage) this.okayButton.getScene().getWindow();
	}

	private ComboBox<EnumShuffleFilter> getGrobSelectionComboBox() {
		return this.grobSelectionComboBox;
	}

	private ComboBox<String> getFeinSelectionComboBox() {
		return this.feinSelectionComboBox;
	}

	public void setScreen(PreferencesScreen screen) {
		this.screen = screen;
	}

	@FXML
	private Button downloadButton;

	@FXML
	private ComboBox<EnumShuffleFilter> grobSelectionComboBox;

	@FXML
	private ComboBox<String> feinSelectionComboBox;

	@FXML
	private CheckBox downloadImagesCheckBox;

	@FXML
	private ProgressBar progressBar;

	@FXML
	private Button proxySettingsButton;

	@FXML
	private Button okayButton;

	@FXML
	private Button abbrechenButton;

	private PreferencesScreen screen;

	private Shuffler shuffler;

	private boolean shufflerChanged = false;

	public void setShuffler(Shuffler shuffler) {
		this.shuffler = shuffler;
	}

}
