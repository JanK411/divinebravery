import de.jjt.divineBravery.gui.PreferencesScreen;
import de.jjt.divineBravery.gui.ShuffleGodScreen;
import de.jjt.divineBravery.util.Constants;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
	public static void main(String[] args) throws IOException {

		final Path dir = Paths.get(Constants.SMITEXML_LOCATION);
		final boolean exists = dir.toFile().exists();

		if (exists) {
			final List<String> files = Files.list(dir)
					.map(Path::getFileName)
					.map(Path::toString)
					.collect(Collectors.toList());

			if (files.contains(Constants.GOD_XML_FILENAME) && files.contains(Constants.ITEM_XML_FILENAME)) {
				javafx.application.Application.launch(ShuffleGodScreen.class);
			}
		} else {
			javafx.application.Application.launch(PreferencesScreen.class);
		}
	}
}