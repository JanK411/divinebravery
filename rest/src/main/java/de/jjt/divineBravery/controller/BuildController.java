package de.jjt.divineBravery.controller;

import de.jjt.divineBravery.model.Ability;
import de.jjt.divineBravery.model.God;
import de.jjt.divineBravery.model.Item;
import de.jjt.divineBravery.util.Build;
import de.jjt.divineBravery.util.Shuffler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BuildController {

	private Shuffler sh = Shuffler.create();

	/**
	 * liefert einen vollständigen Build.
	 *
	 * @param userid
	 * 		customUserId. sollte Jemand einstellungen vornehmen, so wird ein eigener Shuffler pro user gehalten, in welchem dedizierte Einstellungen
	 * 		gemacht werden können. //TODO implement!
	 *
	 * @return vollständiger Itembuild
	 */
	@RequestMapping("/buildRequest")
	public Build buildRequest(@RequestParam(value = "userId", defaultValue = "0") int userid) {
		return sh.getCompleteBuild();
	}

	// nur zu testzwecken
	@RequestMapping("/god")
	public God godRequest() {
		return sh.getCompleteBuild().getGod();
	}

	@RequestMapping("/item")
	public Item itemRequest() {
		return sh.getCompleteBuild().getItem1();
	}

	@RequestMapping("/ability")
	public Ability abilityRequest() {
		return sh.getCompleteBuild().getAbility();
	}
}
