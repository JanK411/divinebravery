package de.jjt.divineBravery.util;

import de.jjt.divineBravery.model.Ability;
import de.jjt.divineBravery.model.God;
import de.jjt.divineBravery.model.Item;
import de.jjt.divineBravery.model.concreteItems.ActiveItem;
import de.jjt.divineBravery.model.enums.EnumLane;

public class Build {
	private final God god;
	private final Ability ability;

	private final ActiveItem relic1;
	private final ActiveItem relic2;

	private final Item boot;
	private final Item item1;
	private final Item item2;
	private final Item item3;
	private final Item item4;
	private final Item item5;

	private final EnumLane lane;

	public Build(final God god, final Ability ability, final ActiveItem relic1, final ActiveItem relic2, final Item boot, final Item item1, final Item item2, final Item item3, final Item item4, final Item item5, final EnumLane lane) {
		this.god = god;
		this.ability = ability;
		this.relic1 = relic1;
		this.relic2 = relic2;
		this.boot = boot;
		this.item1 = item1;
		this.item2 = item2;
		this.item3 = item3;
		this.item4 = item4;
		this.item5 = item5;
		this.lane = lane;
	}

	public God getGod() {
		return god;
	}

	public Ability getAbility() {
		return ability;
	}

	public ActiveItem getRelic1() {
		return relic1;
	}

	public ActiveItem getRelic2() {
		return relic2;
	}

	public Item getBoot() {
		return boot;
	}

	public Item getItem1() {
		return item1;
	}

	public Item getItem2() {
		return item2;
	}

	public Item getItem3() {
		return item3;
	}

	public Item getItem4() {
		return item4;
	}

	public Item getItem5() {
		return item5;
	}

	public EnumLane getLane() {
		return lane;
	}
}
