package de.jjt.divineBravery.util.smiteAPIWrapper;

public enum LANGUAGECODE {
	ENGLISH("1"), GERMAN("2"), FRENCH("3"), SPANISH("7"), SPANISH_LATIN_AMERICA("9"), PORTUGUESE("10"), RUSSIAN(
			"11"), POLISH("12"), TURKISH("13");

	private final String numVal;

	LANGUAGECODE(String numVal) {
		this.numVal = numVal;
	}

	public String getCode() {
		return this.numVal;
	}
}