package de.jjt.divineBravery.util.observer;

import java.util.Collection;
import java.util.LinkedList;

public abstract class Observee {
	private final Collection<Observer> observers = new LinkedList<>();

	public void register(final Observer obs) {
		this.observers.add(obs);
	}

	public void deregister(final Observer obs) {
		this.observers.remove(obs);
	}

	public void notifyObservers(final double percentage) {
		this.observers.forEach(o -> o.update(percentage));
	}

}
