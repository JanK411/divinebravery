package de.jjt.divineBravery.util;

public class Constants {
	public static final String SMITEXML_LOCATION = "Docs/";
	public static final String GOD_XML_FILENAME = "gods.xml";
	public static final String ITEM_XML_FILENAME = "items.xml";
}
