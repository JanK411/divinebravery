package de.jjt.divineBravery.util.fileHelper;

import de.jjt.divineBravery.model.God;
import de.jjt.divineBravery.model.Item;
import de.jjt.divineBravery.util.Constants;
import de.jjt.divineBravery.util.observer.Observer;
import de.jjt.divineBravery.util.smiteAPIWrapper.CouldNotCreateValidSessionException;
import de.jjt.divineBravery.util.smiteAPIWrapper.LANGUAGECODE;
import de.jjt.divineBravery.util.smiteAPIWrapper.SmiteAPIWrapper;
import de.jjt.framework.util.ProxyConfig;

import java.io.IOException;
import java.util.List;

public class Downloader {

	private Downloader() {
	}

	private static Downloader instance;

	/**
	 * Singleton
	 *
	 * @return Liefert die Instanz des Downloaders
	 */
	public static Downloader getInstance() {
		if (instance == null) {
			instance = new Downloader();
		}
		return instance;
	}

	/**
	 * Führt Downloads aus die nötig sind um eine XML-Datei daraus zu
	 * generieren, welche alle relevanten Informationen über alle Götter und
	 * Items enthält die momentan auf smite.guru vorhanden sind.
	 *
	 * @param proxyConfig
	 * 		ProxyConfig um durch einen Proxy den Download zu starten.
	 * @param downloadImages
	 * 		boolean ob Bilder mit Heruntergeladen werden sollen.
	 * @param observer
	 * 		Observer der sich registrieren muss um Informationen über den
	 * 		Downloadfortschritt zu erlangen
	 */
	public void downloadAndGenerateXML(ProxyConfig proxyConfig, boolean downloadImages, Observer observer)
			throws IOException, CouldNotCreateValidSessionException {
		SmiteAPIWrapper smiteAPIWrapper = new SmiteAPIWrapper(proxyConfig);

		List<God> gods = smiteAPIWrapper.getGods(LANGUAGECODE.ENGLISH, downloadImages);
		List<Item> items = smiteAPIWrapper.getItems(LANGUAGECODE.ENGLISH, downloadImages);

		PersistManager writer = new PersistManager(Constants.SMITEXML_LOCATION);
		writer.createSmiteXml(gods, items);
	}

}
