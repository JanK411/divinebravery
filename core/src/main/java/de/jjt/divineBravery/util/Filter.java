package de.jjt.divineBravery.util;

import de.jjt.divineBravery.model.God;
import de.jjt.divineBravery.model.GodVisitor;
import de.jjt.divineBravery.model.Item;
import de.jjt.divineBravery.model.ItemVisitor;
import de.jjt.divineBravery.model.concreteGods.Assassin;
import de.jjt.divineBravery.model.concreteGods.Guardian;
import de.jjt.divineBravery.model.concreteGods.Hunter;
import de.jjt.divineBravery.model.concreteGods.Mage;
import de.jjt.divineBravery.model.concreteGods.Warrior;
import de.jjt.divineBravery.model.concreteItems.ActiveItem;
import de.jjt.divineBravery.model.concreteItems.MagicalItem;
import de.jjt.divineBravery.model.concreteItems.NoneItem;
import de.jjt.divineBravery.model.concreteItems.PhysicalItem;
import de.jjt.divineBravery.model.concreteItems.ShoeMagical;
import de.jjt.divineBravery.model.concreteItems.ShoePhysical;
import de.jjt.divineBravery.model.concreteItems.SpecialItem;
import de.jjt.divineBravery.model.enums.EnumGodRole;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

class Filter {

	private final List<Item> items;

	Filter(List<Item> items) {
		this.items = items;
	}

	/**
	 * entfernt aus einer Liste an Items bestimmte Elemente.
	 *
	 * @param items
	 * 		Liste aus welcher Einträge gelöscht werden sollen.
	 * @param forbiddenItems
	 * 		Items welche aus der Liste entfernt werden sollen.
	 *
	 * @return die beschnittene Itemliste.
	 */
	List<Item> filterForbidden(List<Item> items, Item[] forbiddenItems) {
		items.removeAll(Arrays.asList(forbiddenItems));
		return items;
	}

	/**
	 * @return alle Actives aus allen Items
	 */
	List<Item> getActives() {
		final List<Item> actives = new LinkedList<>();
		for (final Item item : this.items) {
			if (item instanceof ActiveItem) {
				actives.add(item);
			}
		}
		return actives;
	}

	/**
	 * liefert alle zu dem Gott passenden Items.
	 *
	 * @param god
	 * 		ausgewählter Gott
	 *
	 * @return .
	 */
	List<Item> getSuitableItems(final God god) {
		final List<Item> suitableItems = new LinkedList<>();
		god.accept(new GodVisitor() {
			@Override
			public void handle(Warrior warrior) {
				suitableItems.addAll(getPhysicalRelatedItemsSuitableForRole(EnumGodRole.Warrior));
			}

			@Override
			public void handle(Mage mage) {
				suitableItems.addAll(getMagicalRelatedItems(EnumGodRole.Mage));
			}

			@Override
			public void handle(Hunter hunter) {
				suitableItems.addAll(getPhysicalRelatedItemsSuitableForRole(EnumGodRole.Hunter));
			}

			@Override
			public void handle(Guardian guardian) {
				suitableItems.addAll(getMagicalRelatedItems(EnumGodRole.Guardian));
			}

			@Override
			public void handle(Assassin assassin) {
				suitableItems.addAll(getPhysicalRelatedItemsSuitableForRole(EnumGodRole.Assassin));
			}
		});
		return suitableItems;
	}

	/**
	 * liefert alle Schuhe mit der ausgewählten Eigenschaft.
	 *
	 * @param god
	 * 		ausgewählte Eigenschaft
	 *
	 * @return .
	 */
	List<Item> getSuitableShoes(God god) {
		final List<Item> suitableItems = new LinkedList<>();
		god.accept(new GodVisitor() {
			@Override
			public void handle(Warrior warrior) {
				suitableItems.addAll(getPhysicalShoes());
			}

			@Override
			public void handle(Mage mage) {
				suitableItems.addAll(getMagicalShoes());
			}

			@Override
			public void handle(Hunter hunter) {
				suitableItems.addAll(getPhysicalShoes());
			}

			@Override
			public void handle(Guardian guardian) {
				suitableItems.addAll(getMagicalShoes());
			}

			@Override
			public void handle(Assassin assassin) {
				suitableItems.addAll(getPhysicalShoes());
			}
		});
		return suitableItems;
	}

	/**
	 * @param role
	 *
	 * @return Alle Items (keine Schuhe) welche von einem God mit Typ "magical"
	 * 		gebaut werden können.
	 */
	private List<Item> getMagicalRelatedItems(final EnumGodRole role) {
		final List<Item> magicalRelated = new LinkedList<>();
		for (final Item item : this.items) {
			item.accept(new ItemVisitor() {
				@Override
				public void handle(SpecialItem item) {
				}

				@Override
				public void handle(ShoePhysical item) {
				}

				@Override
				public void handle(ShoeMagical item) {
				}

				@Override
				public void handle(PhysicalItem item) {
				}

				@Override
				public void handle(NoneItem item) {
					if (item.isBuildableByRole(role)) {
						magicalRelated.add(item);
					}
				}

				@Override
				public void handle(MagicalItem item) {
					if (item.isBuildableByRole(role)) {
						magicalRelated.add(item);
					}
				}

				@Override
				public void handle(ActiveItem item) {
				}
			});
		}
		return magicalRelated;
	}

	/**
	 * @param role
	 *
	 * @return Alle Items (keine Schuhe) welche von einem God mit Typ "physical"
	 * 		gebaut werden können.
	 */
	private List<Item> getPhysicalRelatedItemsSuitableForRole(final EnumGodRole role) {
		final List<Item> physicalRelated = new LinkedList<>();
		for (final Item item : this.items) {
			item.accept(new ItemVisitor() {
				@Override
				public void handle(SpecialItem item) {
				}

				@Override
				public void handle(ShoePhysical item) {
				}

				@Override
				public void handle(ShoeMagical item) {
				}

				@Override
				public void handle(PhysicalItem item) {
					if (item.isBuildableByRole(role)) {
						physicalRelated.add(item);
					}
				}

				@Override
				public void handle(NoneItem item) {
					if (item.isBuildableByRole(role)) {
						physicalRelated.add(item);
					}
				}

				@Override
				public void handle(MagicalItem item) {
				}

				@Override
				public void handle(ActiveItem item) {
				}
			});
		}
		return physicalRelated;
	}

	/**
	 * @return Alle Schuhe welche von einem God mit Typ "physical" gebaut werden
	 * 		können.
	 */
	private List<Item> getPhysicalShoes() {
		final List<Item> physicalShoes = new LinkedList<>();
		for (final Item item : this.items) {
			if (item instanceof ShoePhysical) {
				physicalShoes.add(item);
			}
		}
		return physicalShoes;
	}

	/**
	 * @return Alle Schuhe welche von einem God mit Typ "magical" gebaut werden
	 * 		können.
	 */
	private List<Item> getMagicalShoes() {
		final List<Item> magicalShoes = new LinkedList<>();
		for (final Item item : this.items) {
			if (item instanceof ShoeMagical) {
				magicalShoes.add(item);
			}
		}
		return magicalShoes;
	}

	/**
	 * Liefert alle Acorns aus den Items.
	 *
	 * @return .
	 */
	Item getTheAcorn() {
		for (final Item item : this.items) {
			if (item instanceof SpecialItem) {
				return item;
			}
		}
		throw new Error("No Acorn found!");
	}

}
