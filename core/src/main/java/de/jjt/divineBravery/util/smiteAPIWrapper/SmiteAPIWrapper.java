package de.jjt.divineBravery.util.smiteAPIWrapper;

import de.jjt.divineBravery.model.Ability;
import de.jjt.divineBravery.model.God;
import de.jjt.divineBravery.model.Item;
import de.jjt.divineBravery.model.concreteGods.Assassin;
import de.jjt.divineBravery.model.concreteGods.Guardian;
import de.jjt.divineBravery.model.concreteGods.Hunter;
import de.jjt.divineBravery.model.concreteGods.Mage;
import de.jjt.divineBravery.model.concreteGods.Warrior;
import de.jjt.divineBravery.model.concreteItems.ActiveItem;
import de.jjt.divineBravery.model.concreteItems.MagicalItem;
import de.jjt.divineBravery.model.concreteItems.NoneItem;
import de.jjt.divineBravery.model.concreteItems.PhysicalItem;
import de.jjt.divineBravery.model.concreteItems.ShoeMagical;
import de.jjt.divineBravery.model.concreteItems.ShoePhysical;
import de.jjt.divineBravery.model.concreteItems.SpecialItem;
import de.jjt.divineBravery.model.enums.EnumGodRole;
import de.jjt.divineBravery.util.Constants;
import de.jjt.divineBravery.util.fileHelper.PersistManager;
import de.jjt.divineBravery.util.observer.Observee;
import de.jjt.framework.fileHelper.Downloader;
import de.jjt.framework.util.ProxyConfig;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class SmiteAPIWrapper extends Observee {

	private static final String BASEURL = "http://api.smitegame.com/smiteapi.svc/";
	private static final String DEVID = "1906";
	private static final String AUTHKEY = "55CC7938F10B42DFAA8AA738FD8A07CE";
	private String sessionId = "";
	private long sessionStart;
	private Downloader imageDownloader;
	private final ProxyConfig proxyConfig;

	// Falls du hier reinschauen solltest @Jan ~ hier die doku:
	// https://docs.google.com/document/d/1OFS-3ocSx-1Rvg4afAnEHlT3917MAK_6eJTR6rzr-BM
	public static void main(String[] args) throws IOException, CouldNotCreateValidSessionException {
		SmiteAPIWrapper apiCalls = new SmiteAPIWrapper();
		List<God> gods = apiCalls.getGods(LANGUAGECODE.ENGLISH, false);
		List<Item> items = apiCalls.getItems(LANGUAGECODE.ENGLISH, false);
		final PersistManager persistManager = new PersistManager(Constants.SMITEXML_LOCATION);
		persistManager.createSmiteXml(gods, items);
		System.out.println(apiCalls.getDataUsed());
	}

	public SmiteAPIWrapper(ProxyConfig proxyConfig) throws CouldNotCreateValidSessionException {

		this.proxyConfig = proxyConfig;
		createSession();
		this.imageDownloader = Downloader.getInstance();
	}

	private SmiteAPIWrapper() throws CouldNotCreateValidSessionException {
		proxyConfig = ProxyConfig.NO_PROXY;
		createSession();
		this.imageDownloader = Downloader.getInstance();
	}

	/**
	 * Creates a Stream of Items, categorized by their appearance (Physical,
	 * Magical, Boots, etc.)
	 *
	 * @param lan
	 * 		LanguageCode - Language of the downloaded data
	 * @param downloadImages
	 * 		boolean if Images should get downloaded
	 *
	 * @return Item-Stream
	 * @throws CouldNotCreateValidSessionException
	 * 		if it wasn't possible to create a Session for this request
	 */
	public List<Item> getItems(LANGUAGECODE lan, boolean downloadImages) throws CouldNotCreateValidSessionException {
		// create session if old one is invalid
		validateSessionId();
		JSONArray itemArray = new JSONArray(makeRequestAndGetResponse("getitems", lan.getCode()));
		System.out.println("SampleItem: " + itemArray.get(0));

		return JSONArrayToJSONObjectStream(itemArray).filter(i -> {
			// Nur Tier3-Items und Actives behalten
			String type = i.getString("Type");
			int tier = i.getInt("ItemTier");
			return (type.equals("Item") && tier == 3 || type.equals("Active") && tier == 2);
		}).map(i -> {
			String name = i.getString("DeviceName");
			if (downloadImages)
				downloadImage(i.getString("itemIcon_URL"), "Images/i_" + name);

			// TODO Bescheidgeb, dass ein Element heruntergeladen wurde
			// notifyObservers();

			// kategorisierung der items
			if (name.contains("Acorn")) {
				// Sonderbehandlung Ratatoskr
				return SpecialItem.create(name);
			} else if (i.getString("Type").equals("Active")) {
				// ist bestimmt ne active
				return ActiveItem.create(name);
			} else if (name.contains("Tabi") || name.contains("Boot") || name.contains("Greave")) {
				// glaube die namen sind momentan eindeutig für physical Schuhe
				return ShoePhysical.create(name);
			} else if (name.contains("Shoe")) {
				// glaube die namen sind momentan eindeutig für magical Schuhe
				return ShoeMagical.create(name);
			} else if (isShittyPhysicalItem(name)) {
				return PhysicalItem.create(name);
			} else {
				// Hier sind es "normale Items"
				JSONObject itemDescription = i.getJSONObject("ItemDescription");
				JSONArray itemStats = itemDescription.getJSONArray("Menuitems");

				// Einschränkungen bestimmter Klassen bei einigen Items herausfinden
				Set<EnumGodRole> restrictedRoles = Arrays.stream(i.getString("RestrictedRoles").split(","))
						.filter(role -> !role.equals("no restrictions") && !role.isEmpty())
						.map(EnumGodRole::valueOfIgnoreCase)
						.collect(Collectors.toSet());

				// API ist inkonsistent - einige items des katana-trees sind nicht als restricted eingetragen, obwohl ingame eine einschränkung besteht -
				// sonderwurst wird hier behandelt.
				if (i.getInt("RootItemId") == (12671)) {
					restrictedRoles.add(EnumGodRole.Hunter);
				}


				Stream<JSONObject> itemStatStream = JSONArrayToJSONObjectStream(itemStats);
				final List<String> relevantStats = itemStatStream.map(stat -> stat.getString("Description"))
						.filter(stat -> stat.equals("Physical Power") || stat.equals("Magical Power"))
						.collect(Collectors.toList());

				//unterscheiden, ob item physical/ magical-only ist, oder ob es von jedem gebaut werden kann.
				if (relevantStats.size() == 1) {
					return relevantStats.get(0).equals("Physical Power") ? PhysicalItem.create(name, restrictedRoles) : MagicalItem.create(name, restrictedRoles);
				} else {
					return NoneItem.create(name, restrictedRoles);
				}
			}
		})
				.distinct()
				.collect(Collectors.toList());
	}

	/**
	 * Creates a Stream of Gods, categorized by their appearance (Hunter, Mage,
	 * etc.)
	 *
	 * @param lan
	 * 		LanguageCode - Language of the downloaded data
	 * @param downloadImages
	 * 		boolean if Images should get downloaded
	 *
	 * @return God-Stream
	 * @throws CouldNotCreateValidSessionException
	 * 		if it wasn't possible to create a Session for this request
	 */
	public List<God> getGods(LANGUAGECODE lan, boolean downloadImages) throws CouldNotCreateValidSessionException {
		validateSessionId();
		JSONArray allGods = new JSONArray(makeRequestAndGetResponse("getgods", lan.getCode()));
		System.out.println("SampleGod: " + allGods.get(0));
		return JSONArrayToJSONObjectStream(allGods).map(jsonGod -> {
			// parst die ersten 3 abilities eines gottes und läd (vielleicht)
			// die Bilder runter
			List<Ability> abilities = IntStream.range(1, 4).mapToObj(i -> {
				String abilityName = jsonGod.getString("Ability" + i);
				if (downloadImages)
					downloadImage(jsonGod.getString("godAbility" + i + "_URL"), "Images/a_" + i + " " + abilityName);

				return new Ability(i + " " + abilityName);
			}).collect(Collectors.toList());

			String name = jsonGod.getString("Name");

			if (downloadImages) {
				String url = jsonGod.getString("godIcon_URL");
				downloadImage(url, "Images/g_" + name);
			}

			// TODO Bescheidgeb, dass ein Element heruntergeladen wurde
			// notifyObservers();

			// god-klasse herausfinden
			switch (jsonGod.getString("Roles").trim()) {
				case "Hunter":
					return Hunter.create(name, abilities);
				case "Assassin":
					return Assassin.create(name, abilities);
				case "Mage":
					return Mage.create(name, abilities);
				case "Guardian":
					return Guardian.create(name, abilities);
				case "Warrior":
					return Warrior.create(name, abilities);
				default:
					// TODO überlegen was man hier schönes machen kann
					return null;
			}
		}).collect(Collectors.toList());
	}

	/**
	 * Checks if the current session is still valid and creates a new one if the
	 * old has expired already. A session is 15min valid
	 *
	 * @throws CouldNotCreateValidSessionException
	 */
	private void validateSessionId() throws CouldNotCreateValidSessionException {
		if (System.currentTimeMillis() - this.sessionStart > 900000) {
			System.out.println("Old session invalid! Creating new one");
			createSession();
		}
	}

	/**
	 * creates Stream of JSONObject if {@code jsonArray} contents are only
	 * JSONObjects
	 *
	 * @param jsonArray
	 *
	 * @return
	 */
	private Stream<JSONObject> JSONArrayToJSONObjectStream(JSONArray jsonArray) {
		// TODO auch hier schöner machen.. glaube das geht aber gar nicht so
		// einfach bei dem JSON framework hier
		List<JSONObject> arrayList = new ArrayList<>();
		jsonArray.forEach(i -> arrayList.add((JSONObject) i));
		return arrayList.stream();
	}

	/**
	 * Downloads Image from {@code url} and creates a persistent file in the
	 * folder specified in {@code fileName}
	 *
	 * @param url
	 * @param fileName
	 */
	private void downloadImage(String url, String fileName) {
		// TODO hier dann später proxyconfig && fancy progressbar
		try {
			imageDownloader.downloadFile(url, fileName + ".jpg");
		} catch (IOException e) {
			System.out.println("Could not download image from the url:\n" + url + "\n with filename: " + fileName);
		}
	}

	/**
	 * @return returns amount of data used today within API
	 * @throws CouldNotCreateValidSessionException
	 */
	private String getDataUsed() {
		return makeRequestAndGetResponse("getdataused");
	}

	/**
	 * @return tests if created Session is valid
	 * @throws CouldNotCreateValidSessionException
	 */
	public String testSession() {
		return makeRequestAndGetResponse("testsession");
	}

	/**
	 * creates mandatory SessionID for any further API calls
	 *
	 * @return SessionId
	 * @throws CouldNotCreateValidSessionException
	 */
	private void createSession() throws CouldNotCreateValidSessionException {
		System.out.println("creating Session");
		String methodCall = "createsession";
		URL url = createURLForRequest(methodCall);
		JSONObject jsonObject = new JSONObject(makeRequestAndGetResponse(url));
		if (jsonObject.getString("ret_msg").equalsIgnoreCase("Approved")) {
			this.sessionId = jsonObject.getString("session_id");
			this.sessionStart = System.currentTimeMillis();
		} else {
			try {
				throw new CouldNotCreateValidSessionException(jsonObject.getString("ret_msg"));
			} catch (JSONException e) {
				throw new Error("Wann tritt das hier auf?");
			}
		}
	}

	/**
	 * liefert true, wenn es sich um eine Sonderregelung handelt und das Item
	 * physical ist.
	 *
	 * @param name
	 * 		einzulesendes Element
	 *
	 * @return .
	 */
	private boolean isShittyPhysicalItem(String name) {
		return name.equals("Odysseus' Bow") || name.equals("Asi") || name.equals("Soul Eater")
				|| name.equals("Ichaival") || name.equals("Silverbranch Bow");
	}

	/**
	 * @return current UTC formatted timestamp for requests to perform
	 */
	private static String getCurrentTimestamp() {
		SimpleDateFormat dateForUTC = new SimpleDateFormat("yyyyMMddHHmmss");
		dateForUTC.setTimeZone(TimeZone.getTimeZone("UTC"));
		return dateForUTC.format(new Date());
	}

	/**
	 * creates a valid Signature necessary to make API calls
	 *
	 * @param methodToCall
	 * 		name of the method to call within the API (without
	 * 		responsetype (e.g. JSON/XML))
	 * @param timeStamp
	 * 		current timeStamp UTC formatted
	 *
	 * @return valid Signature as String
	 */
	private static String createSignature(String methodToCall, String timeStamp) {
		try {
			String stringToHash = DEVID + methodToCall + AUTHKEY + timeStamp;
			byte[] digest = MessageDigest.getInstance("MD5").digest(stringToHash.getBytes());

			StringBuilder stringBuilder = new StringBuilder();
			for (byte b : digest) {
				String hexString = Integer.toHexString(0xff & b);
				if (hexString.length() == 1)
					stringBuilder.append("0");
				stringBuilder.append(hexString);
			}

			return stringBuilder.toString();
		} catch (NoSuchAlgorithmException e) {
			throw new Error("Passiert nie, da MD5 ein gültiger Algorithmus ist");
		}

	}

	/**
	 * @param methodToCall
	 * 		name of the method to call within the API (without
	 * 		responsetype (e.g. JSON/XML))
	 *
	 * @return URL to open a connection
	 */
	private URL createURLForRequest(String methodToCall, String... additionalParamsToAppend) {
		String timeStamp = getCurrentTimestamp();
		String SLASH = "/";
		boolean isCreateSession = methodToCall.equalsIgnoreCase("createsession");
		try {
			StringBuilder stringBuilder = new StringBuilder().append(BASEURL).append(methodToCall).append("json/")
					.append(DEVID).append(SLASH).append(createSignature(methodToCall, timeStamp)).append(SLASH);
			if (!isCreateSession) {
				stringBuilder.append(this.sessionId).append(SLASH);
			}
			stringBuilder.append(timeStamp);

			// jeder zusätzliche parameter wird der URL angefügt
			Arrays.stream(additionalParamsToAppend).forEach(param -> stringBuilder.append(SLASH).append(param));

			return new URL(stringBuilder.toString());
		} catch (MalformedURLException e) {
			throw new RuntimeException("Die zu rufende Methode besitzt ungültige Zeichen! "
					+ "Hier sollen nur die Namen der zu rufenden Services aus der SmiteAPI verwendet werden --> "
					+ methodToCall);
		}

	}

	/**
	 * creates request with {@code param} an {@code additionalParamsToApped}
	 *
	 * @param param
	 * @param additionalParamsToAppend
	 *
	 * @return JSONString
	 */
	private String makeRequestAndGetResponse(String param, String... additionalParamsToAppend) {
		return makeRequestAndGetResponse(createURLForRequest(param, additionalParamsToAppend));
	}

	/**
	 * creates a request with {@code url}
	 *
	 * @param url
	 *
	 * @return JSONString
	 */
	private String makeRequestAndGetResponse(URL url) {
		try {
			System.out.println("used URL for Request: " + url);
			InputStream openStream = url.openConnection(proxyConfig.getJavaNetProxy()).getInputStream();
			BufferedReader buffer = new BufferedReader(new InputStreamReader(openStream));
			String JsonString = buffer.lines().collect(Collectors.joining());
			openStream.close();
			return JsonString;
		} catch (IOException e) {
			throw new InvalidParamsForThisRequestException();
		}
	}
}
