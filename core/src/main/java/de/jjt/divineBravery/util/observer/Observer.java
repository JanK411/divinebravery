package de.jjt.divineBravery.util.observer;

public interface Observer {
	void update(double percentage);
}
