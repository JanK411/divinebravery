package de.jjt.divineBravery.util.smiteAPIWrapper;

public class CouldNotCreateValidSessionException extends Exception {

	public CouldNotCreateValidSessionException(String message) {
		super(message);
	}

	/**
	 *
	 */
	private static final long serialVersionUID = 5999205779017392841L;

}
