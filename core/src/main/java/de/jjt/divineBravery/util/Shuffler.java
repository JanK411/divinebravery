package de.jjt.divineBravery.util;

import de.jjt.divineBravery.model.Ability;
import de.jjt.divineBravery.model.God;
import de.jjt.divineBravery.model.Item;
import de.jjt.divineBravery.model.concreteItems.ActiveItem;
import de.jjt.divineBravery.model.concreteItems.ShoeItem;
import de.jjt.divineBravery.model.enums.EnumGodRole;
import de.jjt.divineBravery.model.enums.EnumLane;
import de.jjt.divineBravery.model.enums.EnumShuffleFilter;
import de.jjt.divineBravery.util.fileHelper.PersistManager;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Klasse zum Mischen der Götter und Items.
 *
 * @author JanK411
 */
public class Shuffler {

	private final Filter filter;

	private final Random rand;

	private final List<God> allGods;

	private List<God> selectedGods;

	private Shuffler(List<Item> items, List<God> gods) {
		this.selectedGods = gods;
		this.allGods = gods;
		this.rand = new Random();
		this.filter = new Filter(items);
	}

	public Build getCompleteBuild() {
		final God god = this.getRandomGod();
		final Ability ability = this.getRandomAbility(god);
		final ActiveItem relic1 = this.getRandomActive();
		final ActiveItem relic2 = this.getRandomActive(relic1);
		final Item boot;
		if (god.getName().equals("Ratatoskr")) {
			boot = this.getTheAcorn();
		} else {
			boot = this.getRandomShoe(god);
		}
		final Item item1 = this.getRandomItem(god);
		final Item item2 = this.getRandomItem(god, item1);
		final Item item3 = this.getRandomItem(god, item1, item2);
		final Item item4 = this.getRandomItem(god, item1, item2, item3);
		final Item item5 = this.getRandomItem(god, item1, item2, item3, item4);
		final EnumLane lane = this.getRandomLane();

		return new Build(god, ability, relic1, relic2, boot, item1, item2, item3, item4, item5, lane);
	}

	/**
	 * liefert ein zufälliges Active. hierbei ausgeschlossen sind alle
	 * forbiddenItems
	 *
	 * @param forbiddenItems
	 * 		ausgeschlossene Items
	 *
	 * @return .
	 */
	private ActiveItem getRandomActive(final ActiveItem... forbiddenItems) {
		List<Item> actives = this.filter.getActives();
		actives = this.filter.filterForbidden(actives, forbiddenItems);

		final int number = this.rand.nextInt(actives.size());
		return (ActiveItem) actives.get(number);
	}

	/**
	 * liefert einen zufälligen Gott.
	 *
	 * @return .
	 */
	private God getRandomGod() {
		final int number = this.rand.nextInt(this.selectedGods.size());
		return this.selectedGods.get(number);
	}

	/**
	 * Sucht die richtigen Einträge die zum filter gehören.
	 *
	 * @param filter
	 * 		filter der GrobFilterCombobox zu welchem passende
	 * 		FeinFilterEinstellungen gefunden werden müssen.
	 *
	 * @return String[] mit den zum filter passenden Einträgen.
	 */
	public String[] getFeinFilterContent(EnumShuffleFilter filter) {
		// TODO gehört eig. gar nicht hier rein sondern ist teil der GUI und sollte rausgezogen werden
		switch (filter) {
			case allGods:
				final String[] allGods = new String[1];
				allGods[0] = "allGods";
				return allGods;
			case god:
				final String[] singleGods = new String[this.allGods.size()];
				for (int i = 0; i < this.allGods.size(); i++) {
					singleGods[i] = this.allGods.get(i).getName();
				}
				return singleGods;
			case role:
				return Arrays.stream(EnumGodRole.values())
						.map(Enum::toString)
						.toArray(String[]::new);
			case type:
				final String[] types = new String[2];
				types[0] = "Physical";
				types[1] = "Magical";
				return types;
		}
		throw new RuntimeException("invalid Enum!");
	}

	/**
	 * liefert ein zufälliges Item, das zu dem Gott passt. ausgeschlossen sind
	 * alle forbiddenItems.
	 *
	 * @param god
	 * 		.
	 * @param forbiddenItems
	 * 		.
	 *
	 * @return .
	 */
	private Item getRandomItem(final God god, final Item... forbiddenItems) {
		List<Item> suitableItems = this.filter.getSuitableItems(god);
		suitableItems = this.filter.filterForbidden(suitableItems, forbiddenItems);

		final int number = this.rand.nextInt(suitableItems.size());
		return suitableItems.get(number);
	}

	/**
	 * liefert einen zufälligen Schuh, passen zu dem ausgewählten Gott.
	 *
	 * @param god
	 * 		ausgewählter Gott
	 *
	 * @return .
	 */
	private ShoeItem getRandomShoe(final God god) {
		final List<Item> shoes = this.filter.getSuitableShoes(god);

		final int number = this.rand.nextInt(shoes.size());
		return (ShoeItem) shoes.get(number);
	}

	/**
	 * liefert ein zufälliges Acorn.
	 *
	 * @return .
	 */
	private Item getTheAcorn() {
		return this.filter.getTheAcorn();
	}

	/**
	 * liefert eine zufällige Abilty, welche zu einem bestimmten God gehört.
	 *
	 * @param god
	 * 		Der God, zu welchem eine Ability geshuffelt werden soll.
	 *
	 * @return die geshuffelte Ability.
	 */
	private Ability getRandomAbility(final God god) {
		final List<Ability> affiliatedAbilities = god.getAbilities();
		final int number = this.rand.nextInt(affiliatedAbilities.size());

		return affiliatedAbilities.get(number);
	}

	/**
	 * liefert eine zufällige Lane
	 *
	 * @return randomLane
	 */
	private EnumLane getRandomLane() {
		final EnumLane[] allLanes = EnumLane.values();
		final int number = this.rand.nextInt(allLanes.length);

		return allLanes[number];
	}

	/**
	 * Setzt die Liste, nach welcher Götter geshuffelt werden, zurück auf die
	 * vollständige Liste.
	 */
	public void resetGodsList() {
		this.selectedGods = this.allGods;
	}

	/**
	 * Setzt die Liste, nach welcher Götter geshuffelt werden, auf einen
	 * einzelnen God.
	 *
	 * @param godName
	 * 		Name des Gottes auf welchen die Liste reduziert werden soll.
	 */
	public void GodsListToGod(String godName) {
		this.selectedGods = this.allGods.stream()
				.filter(god -> god.getName().equalsIgnoreCase(godName))
				.collect(Collectors.toList());
	}

	/**
	 * Setzt die Liste, nach welcher Götter geshuffelt werden, auf alle Götter
	 * mit der rolle {@code role}.
	 *
	 * @param role
	 * 		Rolle der Götter auf welche die Liste reduziert werden soll.
	 */
	public void GodsListToRole(String role) {
		final EnumGodRole godRole = EnumGodRole.valueOfIgnoreCase(role);
		this.selectedGods = this.allGods.stream()
				.filter(god -> god.getRole().equals(godRole))
				.collect(Collectors.toList());
	}

	/**
	 * Setzt die Liste, nach welcher Götter geshuffelt werden, auf alle Götter
	 * mit dem Typen {@code type}.
	 *
	 * @param type
	 * 		this.selectedGods = new Stack<>();
	 * 		Typ der Götter auf welche die Liste reduziert werden soll.
	 */
	public void GodsListToType(String type) {
		this.selectedGods = this.allGods.stream()
				.filter(god -> god.getType().equalsIgnoreCase(type))
				.collect(Collectors.toList());
	}

	/**
	 * erstellt einen neuen Shuffler, welcher die (bestenfalls) bestehende
	 * SmiteXML ausliest und die korrekten Werte zuweist.
	 *
	 * @return der frisch erstellte funktionierende Shuffler.
	 */
	public static Shuffler create() {
		final PersistManager reader = new PersistManager(Constants.SMITEXML_LOCATION);
		List<Item> items = reader.readItems();
		List<God> gods = reader.readGods();
		return new Shuffler(items, gods);
	}

}
