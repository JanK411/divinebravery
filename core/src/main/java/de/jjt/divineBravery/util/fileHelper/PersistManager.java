package de.jjt.divineBravery.util.fileHelper;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;
import de.jjt.divineBravery.model.Ability;
import de.jjt.divineBravery.model.God;
import de.jjt.divineBravery.model.Item;
import de.jjt.divineBravery.util.Constants;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@SuppressWarnings("unchecked")
public class PersistManager {

	private final String godXmlLocation;
	private final String itemXmlLocation;

	private final XStream xStream;

	public PersistManager(final String location) {
		this.godXmlLocation = location + Constants.GOD_XML_FILENAME;
		this.itemXmlLocation = location + Constants.ITEM_XML_FILENAME;
		this.xStream = new XStream();

		XStream.setupDefaultSecurity(this.xStream);
		this.xStream.allowTypesByWildcard(new String[]{
				"de.jjt.divineBravery.model.*",
				"de.jjt.divineBravery.model.concreteItems.*",
				"de.jjt.divineBravery.model.concreteGods.*"
		});

		this.xStream.aliasPackage("i", "de.jjt.divineBravery.model.concreteItems");
		this.xStream.aliasPackage("g", "de.jjt.divineBravery.model.concreteGods");
		this.xStream.aliasPackage("e", "de.jjt.divineBravery.model.enums");
		this.xStream.aliasPackage("a", "de.jjt.divineBravery.model");
		this.xStream.addImplicitCollection(God.class, "abilities");
		this.xStream.registerConverter(new AbiltyConverter());
	}

	public void createSmiteXml(final List<God> gods, final List<Item> items) throws IOException {
		final String godsXML = this.xStream.toXML(gods);
		final String itemsXML = this.xStream.toXML(items);

		final Path path = Paths.get(Constants.SMITEXML_LOCATION);
		if (!Files.exists(path)) {
			Files.createDirectories(path);
		}

		Files.write(Paths.get(godXmlLocation), godsXML.getBytes());
		Files.write(Paths.get(itemXmlLocation), itemsXML.getBytes());
	}

	public List<Item> readItems() {
		return (List<Item>) this.xStream.fromXML(new File(itemXmlLocation));
	}

	public List<God> readGods() {
		return (List<God>) this.xStream.fromXML(new File(godXmlLocation));
	}

	// ************ Magische Converter, um das XML etwas aufgeräumter zu halten ************
	private class AbiltyConverter extends AbstractSingleValueConverter {

		@Override
		public boolean canConvert(final Class aClass) {
			return aClass.equals(Ability.class);
		}

		@Override
		public Object fromString(final String s) {
			return new Ability(s);
		}
	}
}

