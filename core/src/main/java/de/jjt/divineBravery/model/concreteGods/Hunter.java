package de.jjt.divineBravery.model.concreteGods;

import de.jjt.divineBravery.model.Ability;
import de.jjt.divineBravery.model.God;
import de.jjt.divineBravery.model.GodVisitor;
import de.jjt.divineBravery.model.enums.EnumGodRole;

import java.util.List;

public class Hunter extends God {

	private Hunter(final String name, final List<Ability> abilities) {
		super(name, abilities);
	}

	public static Hunter create(final String name, final List<Ability> abilities) {
		return new Hunter(name, abilities);
	}

	@Override
	public EnumGodRole getRole() {
		return EnumGodRole.Hunter;
	}

	@Override
	public void accept(GodVisitor v) {
		v.handle(this);
	}

	@Override
	public String getType() {
		return "physical";
	}
}
