package de.jjt.divineBravery.model.concreteItems;

import de.jjt.divineBravery.model.Item;
import de.jjt.divineBravery.model.ItemVisitor;
import de.jjt.divineBravery.model.enums.EnumGodRole;

import java.util.HashSet;
import java.util.Set;

public class MagicalItem extends Item {

	private MagicalItem(final String name, final Set<EnumGodRole> restrictedRoles) {
		super(name, restrictedRoles);
	}

	public static MagicalItem create(final String name) {
		return new MagicalItem(name, new HashSet<>());
	}

	public static MagicalItem create(final String name, final Set<EnumGodRole> restrictedRoles) {
		return new MagicalItem(name, restrictedRoles);
	}

	@Override
	public void accept(ItemVisitor v) {
		v.handle(this);
	}

	@Override
	public String getType() {
		return "magical";
	}

}
