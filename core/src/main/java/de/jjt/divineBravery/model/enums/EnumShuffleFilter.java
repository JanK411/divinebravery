package de.jjt.divineBravery.model.enums;

/**
 * Kriterien zum Filtern der zu Shuffelnden der Items.
 */
public enum EnumShuffleFilter {
	allGods, type, role, god
}
