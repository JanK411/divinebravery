package de.jjt.divineBravery.model.concreteGods;

import de.jjt.divineBravery.model.Ability;
import de.jjt.divineBravery.model.God;
import de.jjt.divineBravery.model.GodVisitor;
import de.jjt.divineBravery.model.enums.EnumGodRole;

import java.util.List;

public class Mage extends God {

	private Mage(final String name, final List<Ability> abilities) {
		super(name, abilities);
	}

	public static Mage create(final String name, final List<Ability> abilities) {
		return new Mage(name, abilities);
	}

	@Override
	public EnumGodRole getRole() {
		return EnumGodRole.Mage;
	}

	@Override
	public void accept(GodVisitor v) {
		v.handle(this);
	}

	@Override
	public String getType() {
		return "magical";
	}

}
