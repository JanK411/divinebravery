package de.jjt.divineBravery.model.concreteItems;

import de.jjt.divineBravery.model.Item;
import de.jjt.divineBravery.model.ItemVisitor;
import de.jjt.divineBravery.model.enums.EnumGodRole;

import java.util.HashSet;
import java.util.Set;

public class NoneItem extends Item {

	private NoneItem(final String name, final Set<EnumGodRole> restrictedRoles) {
		super(name, restrictedRoles);
	}

	public static NoneItem create(final String name) {
		return new NoneItem(name, new HashSet<>());
	}

	public static NoneItem create(final String name, final Set<EnumGodRole> restrictedRoles) {
		return new NoneItem(name, restrictedRoles);
	}

	@Override
	public void accept(ItemVisitor v) {
		v.handle(this);
	}

	@Override
	public String getType() {
		return "none";
	}

}
