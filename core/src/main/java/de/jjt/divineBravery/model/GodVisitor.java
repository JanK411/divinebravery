package de.jjt.divineBravery.model;

import de.jjt.divineBravery.model.concreteGods.Assassin;
import de.jjt.divineBravery.model.concreteGods.Guardian;
import de.jjt.divineBravery.model.concreteGods.Hunter;
import de.jjt.divineBravery.model.concreteGods.Mage;
import de.jjt.divineBravery.model.concreteGods.Warrior;

public interface GodVisitor {

	void handle(Assassin assassin);

	void handle(Guardian guardian);

	void handle(Hunter hunter);

	void handle(Mage mage);

	void handle(Warrior warrior);

}
