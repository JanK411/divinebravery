package de.jjt.divineBravery.model;

import java.io.File;

/**
 * repräsentiert eine Ability, die ein Gott haben kann.
 */
public class Ability extends SmiteElement {

	/**
	 * Konstruktor der Klasse.
	 *
	 * @param name
	 * 		Name der Ability
	 */
	public Ability(final String name) {
		super(name);
	}

	@Override
	public File getImageFile() {
		return new File("Images/a_" + this.getName() + ".jpg");
	}

	@Override
	public String toString() {
		return this.getName();
	}

	@Override
	public String getType() {
		return "ability";
	}

	@Override
	public String zaubereDefaultImageFileUrl() {
		return "defaultability.png";
	}

}
