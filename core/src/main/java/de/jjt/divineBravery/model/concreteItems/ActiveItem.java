package de.jjt.divineBravery.model.concreteItems;

import de.jjt.divineBravery.model.Item;
import de.jjt.divineBravery.model.ItemVisitor;

public class ActiveItem extends Item {

	private ActiveItem(String name) {
		super(name);
	}

	public static ActiveItem create(String name) {
		return new ActiveItem(name);
	}

	@Override
	public void accept(ItemVisitor v) {
		v.handle(this);
	}

	@Override
	public String getType() {
		return "active";
	}

}
