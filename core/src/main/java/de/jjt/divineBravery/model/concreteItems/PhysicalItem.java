package de.jjt.divineBravery.model.concreteItems;

import de.jjt.divineBravery.model.Item;
import de.jjt.divineBravery.model.ItemVisitor;
import de.jjt.divineBravery.model.enums.EnumGodRole;

import java.util.HashSet;
import java.util.Set;

public class PhysicalItem extends Item {

	private PhysicalItem(final String name, final Set<EnumGodRole> restrictedRoles) {
		super(name, restrictedRoles);
	}

	public static PhysicalItem create(final String name) {
		return new PhysicalItem(name, new HashSet<>());
	}

	public static PhysicalItem create(final String name, final Set<EnumGodRole> restrictedRoles) {
		return new PhysicalItem(name, restrictedRoles);
	}

	@Override
	public void accept(ItemVisitor v) {
		v.handle(this);
	}

	@Override
	public String getType() {
		return "physical";
	}

}
