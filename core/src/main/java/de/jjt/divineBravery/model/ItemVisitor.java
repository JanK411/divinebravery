package de.jjt.divineBravery.model;

import de.jjt.divineBravery.model.concreteItems.ActiveItem;
import de.jjt.divineBravery.model.concreteItems.MagicalItem;
import de.jjt.divineBravery.model.concreteItems.NoneItem;
import de.jjt.divineBravery.model.concreteItems.PhysicalItem;
import de.jjt.divineBravery.model.concreteItems.ShoeMagical;
import de.jjt.divineBravery.model.concreteItems.ShoePhysical;
import de.jjt.divineBravery.model.concreteItems.SpecialItem;

public interface ItemVisitor {

	void handle(ActiveItem item);

	void handle(MagicalItem item);

	void handle(NoneItem item);

	void handle(PhysicalItem item);

	void handle(ShoeMagical item);

	void handle(ShoePhysical item);

	void handle(SpecialItem item);
}
