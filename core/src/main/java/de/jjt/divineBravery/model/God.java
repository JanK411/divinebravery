package de.jjt.divineBravery.model;

import de.jjt.divineBravery.model.enums.EnumGodRole;

import java.io.File;
import java.util.List;

/**
 * Klasse, die einen Smite-God repräsentiert.
 *
 * @author JanK411
 */
public abstract class God extends SmiteElement {

	private final List<Ability> abilities;

	/**
	 * Konstruktor der Klasse.
	 *
	 * @param name
	 * 		Name des Gottes
	 * @param abilities
	 * 		Fähigkeiten des Gottes
	 */
	protected God(final String name, final List<Ability> abilities) {
		super(name);
		this.abilities = abilities;
	}

	@Override
	public File getImageFile() {
		return new File("Images/g_" + this.getName() + ".jpg");
	}

	/**
	 * @return the abilities
	 */
	public List<Ability> getAbilities() {
		return this.abilities;
	}

	/**
	 * liefert die Rolle/ Klasse des jeweiligen Gottes.
	 *
	 * @return mage, guardian, assassin, hunter, warrior
	 */
	public abstract EnumGodRole getRole();

	public abstract void accept(GodVisitor v);


	@Override
	public String zaubereDefaultImageFileUrl() {
		return "defaultgod.png";
	}
}
