package de.jjt.divineBravery.model;

import de.jjt.divineBravery.model.enums.EnumGodRole;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

/**
 * Klasse, die ein Smite-Item repräsentiert.
 *
 * @author JanK411
 */
public abstract class Item extends SmiteElement {

	private final Set<EnumGodRole> restrictedRoles;

	/**
	 * Konstruktor der Klasse.
	 *
	 * @param name
	 * 		Name des Items
	 */
	protected Item(final String name) {
		this(name, new HashSet<>());
	}

	protected Item(final String name, Set<EnumGodRole> restrictedRoles) {
		super(name);
		this.restrictedRoles = restrictedRoles;
	}

	@Override
	public File getImageFile() {
		return new File("Images/i_" + this.getName() + ".jpg");
	}

	public abstract void accept(ItemVisitor v);

	@Override
	public abstract String getType();

	@Override
	public String zaubereDefaultImageFileUrl() {
		return "defaultitem.png";
	}

	public boolean isBuildableByRole(final EnumGodRole role) {
		return !restrictedRoles.contains(role);
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) return true;
		if (!(o instanceof Item)) return false;

		final Item item = (Item) o;

		return this.getName().equals(item.getName());
	}

	@Override
	public int hashCode() {
		return this.getName().hashCode();
	}
}
