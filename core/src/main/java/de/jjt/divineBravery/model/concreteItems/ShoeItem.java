package de.jjt.divineBravery.model.concreteItems;

import de.jjt.divineBravery.model.Item;

public abstract class ShoeItem extends Item {

	ShoeItem(final String name) {
		super(name);
	}
}
