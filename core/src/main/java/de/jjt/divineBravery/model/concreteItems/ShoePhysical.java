package de.jjt.divineBravery.model.concreteItems;

import de.jjt.divineBravery.model.ItemVisitor;

public class ShoePhysical extends ShoeItem {

	private ShoePhysical(final String name) {
		super(name);
	}

	public static ShoePhysical create(final String name) {
		return new ShoePhysical(name);
	}

	@Override
	public void accept(ItemVisitor v) {
		v.handle(this);
	}

	@Override
	public String getType() {
		return "shoePhysical";
	}

}
