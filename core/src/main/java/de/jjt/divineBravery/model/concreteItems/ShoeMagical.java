package de.jjt.divineBravery.model.concreteItems;

import de.jjt.divineBravery.model.ItemVisitor;

public class ShoeMagical extends ShoeItem {

	private ShoeMagical(final String name) {
		super(name);
	}

	public static ShoeMagical create(final String name) {
		return new ShoeMagical(name);
	}

	@Override
	public void accept(ItemVisitor v) {
		v.handle(this);
	}

	@Override
	public String getType() {
		return "shoeMagical";
	}

}
