package de.jjt.divineBravery.model;

import java.io.File;

/**
 * abstrakte Klasse, die Götter und Items vereinigt.
 */
public abstract class SmiteElement {

	/**
	 * Name des Smite-Elements.
	 */
	private final String name;

	/**
	 * Konstruktor der Klasse.
	 *
	 * @param name
	 * 		Name des Elements
	 */
	SmiteElement(final String name) {
		this.name = name;
	}

	/**
	 * liefert den Namen des Elements.
	 *
	 * @return .
	 */
	public String getName() {
		return this.name;
	}

	@Override
	public String toString() {
		return this.name;
	}

	/**
	 * Liefert den lokalen Pfad zum abgespeicherten Bild des SmiteElements.
	 *
	 * @return .
	 */
	public abstract File getImageFile();

	/**
	 * Liefert den Typen des Gottes/ Items als String zurück.
	 *
	 * @return .
	 */
	public abstract String getType();

	/**
	 * @return Liefert das DefaultImage zum aufrufenden SmiteElement.
	 */
	public abstract String zaubereDefaultImageFileUrl(); // Darf nicht mit get beginnen, da Spring diese operation ansonsten als Feld mit über die leitung schiebt
}
