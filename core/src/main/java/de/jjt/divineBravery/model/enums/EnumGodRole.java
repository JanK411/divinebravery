package de.jjt.divineBravery.model.enums;

public enum EnumGodRole {
	Assassin, Hunter, Warrior, Mage, Guardian;

	public static EnumGodRole valueOfIgnoreCase(String name) {
		return EnumGodRole.valueOf(name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase());
	}
}
