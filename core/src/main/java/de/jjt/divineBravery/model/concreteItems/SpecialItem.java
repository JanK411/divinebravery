package de.jjt.divineBravery.model.concreteItems;

import de.jjt.divineBravery.model.Item;
import de.jjt.divineBravery.model.ItemVisitor;

public class SpecialItem extends Item {

	private SpecialItem(final String name) {
		super(name);
	}

	public static SpecialItem create(final String name) {
		return new SpecialItem(name);
	}

	@Override
	public void accept(ItemVisitor v) {
		v.handle(this);
	}

	@Override
	public String getType() {
		return "special";
	}
}
