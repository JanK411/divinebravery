package de.jjt.divineBravery.model.enums;

/**
 * Enum, welches die lanes im Spiel repräsentiert.
 */
public enum EnumLane {
	duo, mid, solo, jungle
}
